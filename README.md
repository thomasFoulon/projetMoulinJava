# Moulin Game

A program which represents a modified tic-tac-toe game.

### How to launch the game

java -jar moulin.jar player1Type player2Type

### Rules of the game

<p>The goal is to be the first to align 3 pawns on a grid of 3x3.</p>
<p>First of all the players are putting their 6 pawns on the grid.
When the 6 pawns are set and no pawns are aligned then
the players can move their pawns to attempt to align them.</p>

### Available player types (playerType)

* Keyboard player,
* Random player (an AI which puts its pawns randomly),
* MiniMax player -> https://en.wikipedia.org/wiki/Minimax

Will you be able to beat the computer ?
